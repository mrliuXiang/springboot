package com.example.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/*该注解包含@SpringBootConfiguration-->
				又包含:@Configuration(表示配置类就是代替了配置文件--spring.xml\springmvc.xml\mybatis.xml)
				表示将该类纳入spring容器中
			@EnableAutoConfiguration-->使springboot可以自动配置.约定优于配置(原则)
				@AutoConfigurationPackage:可以找到主配置类@SpringBootApplication所在的包,作用:可以将该包及子包纳入springIOC容器中
					eg:com.cn.test.Controller手动写到scan扫描器中(springIOC容器)
				spring boot 自动配置 将com.cn.test 及 子包 纳入到spring容器中
				@Import:主要作用引用第三方依赖,在springboot启动时会根据: "META-INF/spring.factories"找到相应的三方依赖并引入本项目
		总结:对自己写的代码以及三方依赖进行配置	,但是springboot可以进行自动配置
		a:自己写的代码:springboot通过@SpringBootConfiguration自动配置
		b:三方依赖:通过spring-boot-autoconfiguration-2.2.6.RELEASE.jar中的META-INF/spring.factories进行声明,
		然后通过@EnableAutoConfiguration开启使用即可.
spring-boot-autoconfiguration-2.2.6.RELEASE.jar包中包含了J2EE整合体系中需要的依赖.
	*/
@MapperScan(basePackages = {"com.example.springboot"})
public class SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
	}

}
/*
* 每一个依赖包都需要一定的条件才会启用*/