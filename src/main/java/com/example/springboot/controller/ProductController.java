package com.example.springboot.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.springboot.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 14:45
 * @Description:
 */
@Controller
@Transactional
public class ProductController {
    @Resource
    private ProductService productService;
    /*
     * records:代表当前分页记录的总数
     * total:不分页的总条数(总行数)
     * size:每页有多少行数据
     * current:当前页
     * searchCount:使用SQL查询记录总数(默认)
     * pages:代表页数
     * */
    @GetMapping("/p")
    @ResponseBody
    public ModelAndView pageProduct(Integer p , Integer r){
        IPage page = productService.getAllProduct(p, r);
        ModelAndView mav = new ModelAndView("/product");
        mav.addObject("productList",page);
        /*page.getPages();
        page.getTotal();
        List<Product> list = page.getRecords();*/
        return mav;
    }
}
