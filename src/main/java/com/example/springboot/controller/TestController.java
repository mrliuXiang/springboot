package com.example.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/27
 * @Time: 13:49
 * @Description:
 */
@Controller
public class TestController {

    @GetMapping("/hello")
    @ResponseBody
    public String  test(){
        return "successawdadawdwadawdadawd";
    }
    @GetMapping("/hello1")
    @ResponseBody
    public String  test1(){
        return "result";
    }
}
