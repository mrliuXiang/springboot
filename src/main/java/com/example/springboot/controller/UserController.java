package com.example.springboot.controller;

import com.example.springboot.entity.User;
import com.example.springboot.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 13:52
 * @Description:
 */
@Controller
@Transactional
@SessionAttributes(value = "user")
public class UserController {
    @Resource
    private UserService userService;
    @GetMapping("/index")
    public String index(){
        return "/login";
    }
    @PostMapping("/login")
    public ModelAndView login(String loginName, String password) {
        String pwd = DigestUtils.md5DigestAsHex(password.getBytes());
        User user = userService.getlogin(loginName, pwd);
        ModelAndView mav = new ModelAndView("/info");
        mav.addObject("user", user);
        return mav;
    }
}
