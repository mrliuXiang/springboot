package com.example.springboot.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springboot.entity.Cart;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 16:40
 * @Description:
 */
@Repository
public interface CartMapper extends BaseMapper<Cart> {

}
