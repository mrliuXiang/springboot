package com.example.springboot.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springboot.entity.Product;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 14:36
 * @Description:
 */
@Repository
public interface ProductMapper extends BaseMapper<Product> {
}
