package com.example.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springboot.entity.Cart;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 16:41
 * @Description:
 */
public interface CartService extends IService<Cart> {
   /* boolean addCart(Integer userid , Integer proid , Integer count);*/
}
