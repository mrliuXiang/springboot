package com.example.springboot.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springboot.entity.Product;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 14:38
 * @Description:
 */
public interface ProductService extends IService<Product> {
    IPage<Product> getAllProduct(Integer page, Integer rows);
}
