package com.example.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.springboot.entity.User;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 13:24
 * @Description:
 */
public interface UserService extends IService<User> {

    User getlogin(String name,String pwd);
}
