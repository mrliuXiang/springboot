package com.example.springboot.service.impl;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springboot.dao.CartMapper;
import com.example.springboot.entity.Cart;
import com.example.springboot.service.CartService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 16:42
 * @Description:
 */
@Service
@Transactional
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements CartService {
  /*  @Resource
    CartMapper cartMapper;
    @Resource
    OrderMapper orderMapper;
    @Resource
    ProductMapper productMapper;

    @Override
    public boolean addCart(Integer userid, Integer productId, Integer count) {
        boolean flag=false;
        if (productId==null || count == 0){
            return false;
        }
        QueryWrapper<Order> con = new QueryWrapper<>();
        con.eq("user_id",userid);
        Order order = orderMapper.selectOne(con);
        if (order==null){
            Order order1 = new Order();
            order1.setUserId(userid);
            orderMapper.insert(order1);
        }
        QueryWrapper<Cart> condition = new QueryWrapper<>();
        condition.eq("order_id",order.getId());
        condition.eq("product_id",productId);
        Cart cart = cartMapper.selectOne(condition);
        if (cart==null){
            Cart cart1 = new Cart();
            cart1.setProductId(productId);
            cart1.setOrderId(order.getId());
            Product product = productMapper.selectById(productId);
            cart1.setQuantity(count);
            cart1.setCost(count*product.getPrice());
            cartMapper.insert(cart1);
            flag= true;
        }else {
            count = cart.getQuantity()+count;
            cart.setQuantity(count);
            cartMapper.updateById(cart);
            flag= true;
        }
        return flag;
    }*/
}
