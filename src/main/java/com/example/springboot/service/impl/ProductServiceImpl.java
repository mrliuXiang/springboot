package com.example.springboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springboot.dao.ProductMapper;
import com.example.springboot.entity.Product;
import com.example.springboot.entity.User;
import com.example.springboot.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 14:44
 * @Description:
 */
@Service
@Transactional
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
    @Resource
    private ProductMapper productMapper;
    /*分页查询商品*/
    @Override
    public IPage<Product> getAllProduct(Integer page, Integer rows) {
        IPage<Product> p = new Page<>(page,rows);
        QueryWrapper<Product> condition = new QueryWrapper<>();
        IPage pageResult= productMapper.selectPage(p,condition);
        return pageResult;
    }
}
