package com.example.springboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springboot.dao.UserMapper;
import com.example.springboot.entity.User;
import com.example.springboot.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/04/30
 * @Time: 13:25
 * @Description:
 */
@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{
    @Resource
    private UserMapper userDao;
    /*登陆*/
    @Override
    public User getlogin(String loginName, String password) {
        QueryWrapper<User> condition = new QueryWrapper<>();
        condition.eq("login_name",loginName);
        condition.eq("password",password);
        return userDao.selectOne(condition);
    }
}
