package com.example.springboot;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootApplicationTests {
    Logger logger = LoggerFactory.getLogger(SpringbootApplicationTests.class);
    @Test
    void contextLoads() {

    }
    @Test
    void logtest() {
        logger.trace("trace---");
        logger.debug("debug----");
        logger.info("info----");
        logger.warn("warn---");
    }
}
